import React, { Component } from 'react';
import { Router, Stack, Scene } from 'react-native-router-flux';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducer from './redux/store/reducers/diseases';
import middleware from './redux/store/middleware';
import Login from './components/loginComponent';
import SignUp from './components/signupComponent'
import Disease from './components/diseaseComponent';
import DiseasesList from './components/diseaseslistComponent';
import SymptomsCheker from './components/checkingsymptomsComponent';
import Logo from './components/logoComponent';
import { registerNotification } from './redux/utils/notificationService';

export default class App extends Component {

  componentDidMount() {
    registerNotification();
  }

  store = createStore(reducer, middleware)

  render() {
    return (
      <Provider store={this.store}>
        <Router>
          <Stack key="root">
          <Scene key="signup" component={SignUp} title="Sign Up" />
          <Scene key="login" component={Login} title="Login" />
          <Scene key="logo" component={Logo} title="Logo" />
          
            <Scene key="diseases" component={DiseasesList} title="Diseases" />      
            <Scene key="disease" component={Disease} title="Disease" />      
            <Scene key="symptomscheker" component={SymptomsCheker} title="Symptoms" />
            
            
            
            
            
          </Stack>
        </Router>
      </Provider>
    );
  }
}
