import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';

export default class Logo extends Component {

    state = {
        error : false
    };

    _onImageLoadError = (event) => {
        console.warn(event.nativeEvent.error);
        this.setState({ error : true });
    }

    render() {
    	const { alt } = "Quick Cure Logo"
        const { error } = this.state;

        if (error) {
            return (
                <Text>{alt}</Text>
            );
        }

        return (
            <Image 
            	style={styles.logo}
                accessible
                accessibilityLabel={alt}
                source={require('../assets/logo.jpeg')}                 
                onError={this._onImageLoadError} />
        );
    }
}
const styles = StyleSheet.create({
logo:{
	width: 120,
    height: 90,
    position: 'relative',
    }
    });