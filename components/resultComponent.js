import React from 'react';
import { StyleSheet, Text, View, TextInput, Dimensions } from 'react-native';
import { resetNotifications, registerNotification } from '../redux/utils/notificationService';
import Button from './buttonComponent';
import Logo from './logoComponent';

export default function Quiz({ goBack, restartQuiz, totalCorrectAns, totalQuestions }) {

    resetNotifications()
        .then(registerNotification);
    return (
        <View style={styles.maincontainer}>
    <View style={styles.logocontainer}>
      <Logo/>
    </View>
    <View style={styles.bodycontainer}>
        <View style={styles.container}>
        <TextInput
        multiline={true}
        numberOfLines={13}
        value={ "You have" + " " + totalCorrectAns + " out of " + totalQuestions + " " + "symptoms" + "\n" + "You have" + " " + ((totalCorrectAns / totalQuestions) * 100).toFixed(2) + " " + "% chance of causing this disease."}
        editable={ false }
        style={ styles.TextInputStyleClass }
        />
        <View style={styles.margin}>
            <Button text="Go Back" err="err" onPress={goBack}/>                    
        </View>
        </View>        
    </View>
        </View>
    );
}

var styles = StyleSheet.create({
    TextInputStyleClass:{

// Set border width.
 borderWidth: 1,
// Set border Hex Color Code Here.
 borderColor: '#FF5722',
width: Dimensions.get('window').width/1.3,

    fontFamily: "serif",
    fontSize: 13,
    padding: 10,
},
maincontainer:{
  flexDirection:'column',
  flex:1,
  alignItems: 'center',
    justifyContent: 'center',
  },
  logocontainer:{
  flexDirection:'column',
  flex:1,
  paddingTop: 10,
  },
  bodycontainer:{
  flexDirection:'column',
  flex:4,
  },
    container: {
        padding: 10,
    },
    margin:{
    marginVertical: 30,
  },
})
