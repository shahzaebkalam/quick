import React, { Component } from 'react';
import { StyleSheet, TouchableHighlight, Text,Alert , View, Dimensions, ScrollView } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { setAllDiseases } from '../redux/store/actions/diseases';
import { getAllDiseases } from '../redux/utils/dataService';
import { getAll } from '../redux/store/actions/diseases';
import Button from './buttonComponent';
import Logo from './logoComponent';



class DiseasesList extends Component{
  
  state = {
        diseases: []
    }
componentDidMount() {

        if (!(this.props.diseases && this.props.diseases.length)) {
            const diseases = getAllDiseases();
            this.props.dispatch(setAllDiseases(diseases));
            this.props.dispatch(getAll());
        }
    }
    

    static getDerivedStateFromProps(props, state) {

        if (props.diseases && props.diseases.length) {
            return { diseases: props.diseases }
        }
        else if (props.diseases && props.diseases.length !== state.diseases)
            return { diseases: [] }
        return null;
    }


  render(){
    const { diseases } = this.state;
        if (!diseases.length)
            return (
                <Text style={styles.warningMsg}>
                    
                    You do not have any diseases in your list to check!
                </Text>)
          return (
          <View style={styles.maincontainer}>
    <View style={styles.logocontainer}>
      <Logo/>
    </View>
    <View style={styles.bodycontainer}>
      <LinearGradient
        colors={['#28586F', '#FE5957' ]}
         style={styles.container}        
        start={{ x: 0.9, y: 0.3 }}
      >
              <ScrollView style={styles.scrollview}>
                        {diseases.map(disease =>
                          <View style={styles.margin}>

                          <Button key={disease.title} text={disease.title} err="err" 
                            onPress={ () => {
                             Alert.alert(
                                'Delete Favorite?',
                                'If you are causing any other diseases, Please do not follow ' + disease.title + ' Symtoms.',
                                [
                                    { 
                                        text: 'Cancel', 
                                        onPress: () => console.log('Not Attempted'),
                                        style: ' cancel'
                                    },
                                    {
                                        text: 'OK',
                                        onPress: () => { Actions.disease({ title: disease.title }) }
                                    }
                                ],
                                { cancelable: false }
                            );
                            
                          }
                        }
                          />
                          </View>
                        )}
                    
              </ScrollView>
    </LinearGradient>
    </View>
    </View>
        );
}
}

const styles = StyleSheet.create({
  scrollview:{
    marginVertical: 20,
    
  },
  maincontainer:{
  flexDirection:'column',
  flex:1,
  alignItems: 'center',
    justifyContent: 'center',
  },
  logocontainer:{
  flexDirection:'column',
  flex:1,
  paddingTop: 10,
  },
  bodycontainer:{
  flexDirection:'column',
  flex:4,
  },
  container: {
    width: Dimensions.get('window').width/1.1,
    height: Dimensions.get('window').height/1.5,
    borderRadius:70,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  warningMsg: {
        fontSize: 18,
        textAlign: 'center',
        marginTop: 20
    },
  margin:{
    marginVertical: 10,
  },
});

function mapStateToProps(diseases) {
    return {
        diseases: Object.values(diseases)
    }
}
export default connect(mapStateToProps)(DiseasesList)