import React, { Component } from 'react';
import { StyleSheet, TouchableHighlight, Text, View, Alert, Dimensions, KeyboardAvoidingView } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import { Actions } from 'react-native-router-flux';
import Button from './buttonComponent';
import TextBox from './textboxComponent';
import Logo from './logoComponent';

class SignUp extends Component{

constructor(props){
    super(props);
    this.state={
        username: "",
        email: "",
        password:"",
        confirm_password: ""
    },
    this.handleChange=this.handleChange.bind(this);
    
}
handleChange(e,name){
  this.setState({
    [name]:e
  });
  console.log(this.state.username);
}

  

  render(){
  return (
    <View style={styles.maincontainer}>
    <View style={styles.logocontainer}>
      <Logo/>
    </View>
    <View style={styles.bodycontainer}>
    <LinearGradient
        colors={['#28586F', '#FE5957' ]}
         style={styles.container}        
        start={{ x: 0.9, y: 0.3 }}
      >
    <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
    >
    <View style={styles.formMargin} >
      <TextBox placeholder="USERNAME" name="username" hide_text="" value={ this.state.username } onChange={ this.handleChange }/>
    </View>
    <View style={styles.formMargin} >
      <TextBox placeholder="EMAIL" name="email" hide_text="" value={ this.state.email } onChange={ this.handleChange }/>
    </View>
    <View style={styles.formMargin}>
      <TextBox placeholder="PASSWORD" name="password" hide_text="true" value={ this.state.password } onChange={ this.handleChange }/>
    </View>
    <View style={styles.formMargin}>
      <TextBox placeholder="CONFIRM PASSWORD" name="confirm_password" hide_text="true" value={ this.state.confirm_password } onChange={ this.handleChange }/>
    </View>
    <View style={styles.formMargin}>
    	<Button text="SIGN UP" err="err" onPress={()=>{ Actions.login() }}/>      
    </View>
    </KeyboardAvoidingView>
    </LinearGradient>
    </View>
    </View>
  );
  }
}
const styles = StyleSheet.create({
  maincontainer:{
    flexDirection:'column',
    flex:1,
    alignItems: 'center',
      justifyContent: 'center',
    },
    logocontainer:{
    flexDirection:'column',
    flex:1,
    paddingTop: 10,
    },
    bodycontainer:{
    flexDirection:'column',
    flex:4,
    },
  container: {
    width: Dimensions.get('window').width/1.1,
    height: Dimensions.get('window').height/1.5,
    borderRadius:70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  formMargin: {
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textColor: {
    color: "#f6fefa",
    fontFamily: "serif",
    fontSize: 10,

  }
});

export default SignUp;