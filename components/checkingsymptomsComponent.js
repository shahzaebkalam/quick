import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import Result from './resultComponent';
import Button from './buttonComponent';
import Logo from './logoComponent';

class SymptomsCheker extends Component {

  state = {
    disease: {},
    questionIndex: 0,
    showAns: false,
    showResult: false,
    correctAns: 0
  }

  static getDerivedStateFromProps(props) {
    if (props.diseases && Object.values(props.diseases).length) {
      const title = props.title;
      return { disease: props.diseases[title] || {} }
    }
    return null;
  }

  handleSubmit = (isCorrect) => {
    this.setState(state => {
      if (state.questionIndex < state.disease.questions.length - 1)
        return {
          questionIndex: state.questionIndex + 1,
          showAns: false,
          correctAns: isCorrect ? state.correctAns + 1 : state.correctAns
        }
      return {
        showResult: true,
        showAns: false,
        correctAns: isCorrect ? state.correctAns + 1 : state.correctAns
      }
    })
  }

  restartQuiz = () => {
    this.setState({
      questionIndex: 0,
      showAns: false,
      showResult: false,
      correctAns: 0
    });
  }

  goBack = () => {
    Actions.pop();
  }

  render() {
    const { showAns, showResult, questionIndex, disease: { questions }, correctAns } = this.state;

    if (showResult)
      return (
        <Result
          restartQuiz={this.restartQuiz}
          goBack={this.goBack}
          totalCorrectAns={correctAns}
          totalQuestions={questions.length} />
      )
    if(questions.length>0)
      return (
        <View style={styles.maincontainer}>
    <View style={styles.logocontainer}>
      <Logo/>
    </View>
    <View style={styles.bodycontainer}>
        <View style={styles.container}>
            <View style={styles.quizContainer}>
              <Text style={styles.count}>
                {questionIndex + 1} out of {questions.length}
              </Text>
              <Text style={styles.question}> {questions[questionIndex].question}</Text>
              <Text style={styles.answer}>{questions[questionIndex].answer}</Text>
            <View style={styles.margin}>
              <Button text="Yes" err="err" onPress={() => { this.handleSubmit(true) }}/>
            </View>
            <View style={styles.margin}>
              <Button text="No" err="err" onPress={() => { this.handleSubmit(false) }}/>
            </View>
            </View>            
            </View> 
          </View>
        </View>
      );
      else{
        return(
          <View style={styles.container}>
            <Text style={styles.noQuestion}>You do not have any Question in this disease.</Text>
            <Button text="Go Back" err="err" onPress={this.goBack}/>
          </View>
        );
      }
  }
}

var styles = StyleSheet.create({
  maincontainer:{
  flexDirection:'column',
  flex:1,
  alignItems: 'center',
  justifyContent: 'center',
  },
  logocontainer:{
  flexDirection:'column',
  flex:1,
  paddingTop: 10,
  },
  bodycontainer:{
  flexDirection:'column',
  flex:3,
  },
  container: {
    padding: 10,
  },
  count: {
    textAlign: "center",
    fontSize: 12,
    marginBottom: 10
  },
  quizContainer: {
    minHeight: 200,
    alignItems: 'center',
    justifyContent: 'center',
  },
  question: {
    fontSize: 19,
    fontWeight: "600"
  },
  answer: {
    fontSize: 16,
    marginTop: 15
  },
  noQuestion: {
    fontSize: 19,
    fontWeight: "600"
  },
  margin:{
    marginVertical: 10,
  },
})

function mapStateToProps(diseases) {
  return {
    diseases
  }
}
export default connect(mapStateToProps)(SymptomsCheker)