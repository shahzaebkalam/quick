import React, { Component } from 'react';
import { StyleSheet, Text, View  } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Button from './buttonComponent';
import Logo from './logoComponent';


class Disease extends Component {

    state = {
        disease: {}
    }
    handleStartQuiz = (title) =>{
        Actions.symptomscheker({title: title});
    }

    
    static getDerivedStateFromProps(props) {
        if (props.diseases && Object.values( props.diseases).length ) {
            const title = props.title;
            return { disease: props.diseases[title] || {} }
        }
        return null;
    }

    render() {
        const { disease } = this.state;
        return (
            <View style={styles.maincontainer}>
    <View style={styles.logocontainer}>
      <Logo/>
    </View>
    <View style={styles.bodycontainer}>
                <View style={styles.container}>
                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>
                            {disease.title}
                        </Text>
                        <Text style={styles.subTitle}>
                            This disease have 
                            {disease.questions && disease.questions.length?
                            " " + disease.questions.length + " ": 0 + " "}
                             Symptoms you can check either you have these symptoms or not.                             
                        </Text>
                    </View>                    
                </View> 
            <Button text="Let's Check" err="err" onPress={()=>this.handleStartQuiz(disease.title)}/>                                   
                </View>
    </View>         
        );
    }
}

const styles = StyleSheet.create({
    maincontainer:{
  flexDirection:'column',
  flex:1,
  alignItems: 'center',
    justifyContent: 'center',
  },
  logocontainer:{
  flexDirection:'column',
  flex:1,
  paddingTop: 10,
  },
  bodycontainer:{
  flexDirection:'column',
  flex:3,
  alignItems: 'center',
  },
    container: {
        padding: 10,
    },
    titleContainer: {
        justifyContent: "flex-start",
        alignItems: "center",
        height: 150
    },
    title: {
        fontSize: 18
    },
    subTitle: {
        fontSize: 15,
        marginTop: 30,
        paddingRight: 5
    },
})
function mapStateToProps(diseases) {
    return {
        diseases
    }
}
export default connect(mapStateToProps)(Disease);