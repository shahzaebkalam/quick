import * as ActionTypes from '../ActionTypes';

export function setAllDiseases(diseases) {
    return {
        type: ActionTypes.SET_ALL_DISEASES,
        diseases
    }
}
export function getAll() {
    return {
        type: ActionTypes.GET_ALL_DISEASES
    }
}
export function addDisease(title) {
    return {
        type: ActionTypes.ADD_DISEASE,
        title
    }
}
export function getDisease(key) {
    return {
        type: ActionTypes.GET_DISEASE,
        title: key
    }
}
export function deleteDisease(key) {
    return {
        type: ActionTypes.DELETE_DISEASE,
        title: key
    }
}
export function addCard(key, question, answer) {
    return {
        type: ActionTypes.ADD_NEW_CARD,
        title: key,
        questionText: question,
        answerText: answer
    }
}