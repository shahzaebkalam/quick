import * as ActionTypes from '../ActionTypes';

let diseases = {};
export default function diseasesReducer(state = {}, action) {
    switch (action.type) {
        case ActionTypes.SET_ALL_DISEASES:
            return Object.assign({}, {...action.diseases})
        case ActionTypes.GET_ALL_DISEASES:
            return {
                ...state
            }
        case ActionTypes.ADD_DISEASE:
            diseases = { ...state }
            diseases[action.title] = {
                title: action.title,
                questions: []
            };
            return Object.assign({}, { ...diseases });
        case ActionTypes.GET_DISEASE:
            diseases = { ...state };
            return diseases.diseases[action.title]
        case ActionTypes.DELETE_DISEASE:
            diseases = { ...state };
            delete diseases[action.title]
            return Object.assign({}, { ...diseases });
        case ActionTypes.ADD_NEW_CARD:
            diseases = { ...state }
            diseases[action.title].questions.push(
                {
                    question: action.questionText,
                    answer: action.answerText
                }
            )
            return Object.assign({}, { ...diseases });

        default:
            return state
    }
}