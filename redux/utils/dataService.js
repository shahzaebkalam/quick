let diseases = {
    Dirhea: {
        title: 'Dirhea',
        questions: [
            {
                question: 'What is React?',
                answer: 'A library for managing user interfaces'
            },
            {
                question: 'Where do you make Ajax requests in React?',
                answer: 'The componentDidMount lifecycle event'
            }
        ]
    },
    Flue: {
        title: 'Flue',
        questions: [
            {
                question: 'What is React?',
                answer: 'A library for managing user interfaces'
            },
            {
                question: 'Where do you make Ajax requests in React?',
                answer: 'The componentDidMount lifecycle event'
            }
        ]
    },
    Pain: {
        title: 'Pain',
        questions: [
            {
                question: 'What is React?',
                answer: 'A library for managing user interfaces'
            },
            {
                question: 'Where do you make Ajax requests in React?',
                answer: 'The componentDidMount lifecycle event'
            }
        ]
    },
    Diabeties: {
        title: 'Diabeties',
        questions: [
            {
                question: 'What is React?',
                answer: 'A library for managing user interfaces'
            },
            {
                question: 'Where do you make Ajax requests in React?',
                answer: 'The componentDidMount lifecycle event'
            }
        ]
    },
    Alergy: {
        title: 'Alergy',
        questions: [
            {
                question: 'What is React?',
                answer: 'A library for managing user interfaces'
            },
            {
                question: 'Where do you make Ajax requests in React?',
                answer: 'The componentDidMount lifecycle event'
            }
        ]
    },
    HeadAch: {
        title: 'Head Ach',
        questions: [
            {
                question: 'What is React?',
                answer: 'A library for managing user interfaces'
            },
            {
                question: 'Where do you make Ajax requests in React?',
                answer: 'The componentDidMount lifecycle event'
            }
        ]
    },
    Cough: {
        title: 'Cough',
        questions: [
            {
                question: 'What is React?',
                answer: 'A library for managing user interfaces'
            },
            {
                question: 'Where do you make Ajax requests in React?',
                answer: 'The componentDidMount lifecycle event'
            }
        ]
    },
}

export function getAllDiseases() {
    return { ...diseases };
}

export function addDisease(key) {
    diseases[key] = {
        title: key,
        questions: []
    };
    return {...diseases[key]};
}

export function deleteDisease(key) {
    delete diseases[key];
}

export function getDisease(key) {
    return diseases[key];
}

export function addCard(key, questionText, answerText) {
    diseases[key].questions.push(
        {
            question: questionText,
            answer: answerText
        }
    )
}